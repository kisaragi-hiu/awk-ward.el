;;; awk-ward.el --- Run and edit Awk programs -*- lexical-binding: t -*-

;; Author: Kisaragi Hiu
;; Maintainer: Kisaragi Hiu
;; Version: 0.5.0
;; Package-Requires: ((emacs "25.1"))
;; Homepage: https://gitlab.com/kisaragi-hiu/awk-ward.el
;; Keywords: editing convenience


;; This file is not part of GNU Emacs

;; didyoumean.el is free software: you can redistribute it and/or modify
;; it under the terms of the MIT License.

;;; Commentary:

;; Try out Awk scripts right in your editor.

;; somewhat broken at the moment.

;;; Code:

(eval-when-compile
  (require 'cl-lib)
  (require 'subr-x))

;;;; Variables

(defgroup awk-ward nil
  "Awk-ward."
  :group 'editing
  :prefix "awk-ward-")

(defcustom awk-ward-command "awk"
  "Name or full path to the Awk executable."
  :group 'awk-ward
  :type 'path)

(defcustom awk-ward-options nil
  "Options for Awk command."
  :group 'awk-ward
  :type '(repeat string))

(defcustom awk-ward-update-on-evil-state-change nil
  ;; affects `awk-ward-mode--turn-on'.
  "Update Awk-ward output only when leaving Evil insert state.

Has no effect if Evil is not installed."
  :group 'awk-ward
  :type 'boolean)

(defvar awk-ward--window-configuration nil
  "Saved window configuration before entering Awk-ward.")

(defvar awk-ward-buffers-default `((program . "*Awk-ward Program*")
                                   (input . "*Awk-ward Input*")
                                   (output . "*Awk-ward Output*"))
  "Alist of default Awk-ward buffer names.")

(defvar awk-ward--buffers-alist awk-ward-buffers-default
  "Alist of Awk-ward buffers.")

;;;; Utils

(cl-deftype awk-ward--not-output ()
  '(or (eql program) (eql input)))

(defun awk-ward--temp-file (type)
  "Return path to Awk-ward's temporary file for TYPE.

TYPE can be `input', `program', or `both'."
  (cl-case type
    (both (list (awk-ward--temp-file 'input)
                (awk-ward--temp-file 'program)))
    (t (expand-file-name (format "awkwardel-%s" (symbol-name type))
                         temporary-file-directory))))

;;;; Display, Cleanup

(defun awk-ward-show ()
  "Show Awk-ward buffers."
  (let-alist awk-ward--buffers-alist
    (display-buffer (get-buffer-create .input))
    (display-buffer (get-buffer-create .output))
    (display-buffer (get-buffer-create .program))))

(defun awk-ward-stop ()
  "Stop Awk-ward.

Also remove Awk-ward buffers and temporary files, and restore
window configuration."
  (interactive)
  (pcase-dolist (`(,_ . ,buf) awk-ward--buffers-alist)
    (when (get-buffer buf)
      (with-current-buffer buf
        (awk-ward-mode -1)
        (kill-buffer (current-buffer)))))
  (mapc #'delete-file (awk-ward--temp-file 'both))
  (when awk-ward--window-configuration
    (set-window-configuration awk-ward--window-configuration))
  (setq awk-ward--window-configuration nil)
  (message "Awk-ward stopped"))

;;;; Change input / program

(defun awk-ward-mark (buf type)
  "Mark BUF as the buffer for TYPE (`program' or `input')."
  (interactive (list (current-buffer)
                     (intern-soft
                      (completing-read "Mark as: " '(program input)))))
  (cl-check-type type awk-ward--not-output)
  (setf (alist-get type awk-ward--buffers-alist) (buffer-name buf)))

;;;; Main update function

(defun awk-ward--run (program input output-buf)
  "Run Awk PROGRAM with INPUT, writing output to OUTPUT-BUF.

Both PROGRAM and INPUT are paths to files."
  (with-current-buffer (get-buffer-create output-buf)
    (delete-region (point-min) (point-max)))
  (make-process
   :name "awk-ward"
   :buffer output-buf
   :command `(,awk-ward-command ,@awk-ward-options "-f" ,program ,input)
   :sentinel #'ignore
   :filter (lambda (process string)
             (with-current-buffer (process-buffer process)
               (insert string)
               (goto-char (point-min))
               (set-marker (process-mark process) (point))))))

(defun awk-ward--update (&optional program-buffer input-buffer output-buffer)
  "Update Awk-ward output.

Take Awk program from PROGRAM-BUFFER, input from INPUT-BUFFER,
and write output to OUTPUT-BUFFER.

Use `awk-ward--buffers-alist' as the fallback if arguments are nil."
  (let-alist awk-ward--buffers-alist
    (unless program-buffer (setq program-buffer .program))
    (unless input-buffer (setq input-buffer .input))
    (unless output-buffer (setq output-buffer .output)))
  (let ((input-file (awk-ward--temp-file 'input))
        (program-file (awk-ward--temp-file 'program)))
    ;; From `write-region':
    ;; If VISIT is neither t nor nil nor a string, do not display the
    ;; "Wrote file" message.
    ;; Wat?
    (with-current-buffer input-buffer
      (write-region (point-min) (point-max) input-file nil :dont))
    (with-current-buffer program-buffer
      (write-region (point-min) (point-max) program-file nil :dont))
    (awk-ward--run program-file input-file output-buffer)))

;;;; Minor mode

(defvar awk-ward-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c $ q") #'awk-ward-stop)
    map)
  "Keymap for Awk-ward buffers.")

(defun awk-ward-mode--turn-on ()
  "Turn on `awk-ward-mode'."
  (if (and (featurep 'evil) awk-ward-update-on-evil-state-change)
      (add-hook 'evil-insert-state-exit-hook #'awk-ward--update nil t)
    (add-hook 'post-self-insert-hook #'awk-ward--update nil t))
  (add-hook 'kill-buffer-hook #'awk-ward-stop nil t))

(defun awk-ward-mode--turn-off ()
  "Turn off `awk-ward-mode'."
  (remove-hook 'evil-insert-state-exit-hook #'awk-ward--update t)
  (remove-hook 'post-self-insert-hook #'awk-ward--update t)
  (remove-hook 'kill-buffer-hook #'awk-ward-stop t))

(define-minor-mode awk-ward-mode
  "Minor mode for Awk-ward buffers."
  :group 'awk-ward :global nil :init-value nil
  (if awk-ward-mode
      (awk-ward-mode--turn-on)
    (awk-ward-mode--turn-off)))

;;;; Entry point; setup function

(defun awk-ward--setup (buffers-alist)
  "Set up Awk-ward visiting buffers in BUFFERS-ALIST.

BUFFERS-ALIST looks like:
\((program . PROGRAM-BUFFER)
  (input . INPUT-BUFFER)
  (output . OUTPUT-BUFFER)"
  (setq awk-ward--buffers-alist buffers-alist)
  (let-alist buffers-alist
    (with-current-buffer (get-buffer-create .program)
      (awk-mode)
      (awk-ward-mode))
    (with-current-buffer (get-buffer-create .input)
      (text-mode)
      (awk-ward-mode))
    (with-current-buffer (get-buffer-create .output)
      (awk-ward-mode))))

;;;###autoload
(defun awk-ward (program-buffer input-buffer)
  "Start Awk-ward looking at PROGRAM-BUFFER and INPUT-BUFFER.

Use current buffer as the program buffer if it's in Awk mode.
Otherwise, use `awk-ward-program-buffer'.

With a \\[universal-argument], prompt for a file to visit as the
INPUT-BUFFER."
  (interactive
   (list (or (and (eq major-mode 'awk-mode)
                  (current-buffer))
             (alist-get 'program awk-ward-buffers-default))
         (or (and current-prefix-arg
                  (find-file-noselect (read-file-name "Input file: " nil nil t)))
             (alist-get 'input awk-ward-buffers-default))))
  (awk-ward-mark program-buffer 'program)
  (awk-ward-mark input-buffer 'input)
  (awk-ward--setup awk-ward--buffers-alist)
  (awk-ward-show))

;;;; Provide

(provide 'awk-ward)

;;; awk-ward.el ends here
