# Awk-ward.el - Run Awk programs in Emacs

Emacs port of [Awk-ward](https://gitlab.com/HiPhish/awk-ward.nvim).

> Awk-ward allows you to try out Awk scripts right in your editor; you provide the input as either a file or a buffer and the output is displayed in an output buffer. Best of all, you don't even have to write your program or data to disc, Awk-ward will read straight from the buffer, and you can experiment without breaking any of your programs.

## Usage

Run `awk-ward`. Three buffers will be presented; put your raw data in `*Awk-ward Input*`, write your Awk program in `*Awk-ward Program*`, and the result will be shown in `*Awk-ward Output*`.

To use an existing file as the input or the Awk program, simply open (`find-file`) it from the corresponding buffers. Alternatively, you can manually mark a buffer with `awk-ward-mark`.

Call `awk-ward-stop` (or kill an Awk-ward.el buffer) to return Emacs back to normal.

## Customize

- `awk-ward-command`: path to the Awk executable.
- `awk-ward-options`: command line options passed to Awk.
- `awk-ward-update-on-evil-state-change`: update Awk output when leaving Evil insert state. No effect if Evil is not installed.

## Project Status

There are some quirks that I’m not motivated enough to fix. Consider this an [“80% project”](http://winestockwebdesign.com/Essays/Lisp_Curse.html).

## License

MIT, see `LICENSE`.
